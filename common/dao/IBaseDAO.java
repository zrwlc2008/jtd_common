package com.jtd.common.dao;

import java.io.Serializable;

public interface IBaseDAO {
	public <T> void save(T object);

	public <T> void update(T object);

	public <T> void delete(T object);

	public <T> void delete(Class<T> entityClass, Serializable id);

	public <T> T get(Class<T> entityClass, Serializable id);

	public <T> void logicDelete(Class<T> entityClass, Serializable id);

	public <T> void updateSelective(T object);
}
