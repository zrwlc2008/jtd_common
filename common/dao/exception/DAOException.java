package com.jtd.common.dao.exception;

import com.jtd.common.CommonRuntimeException;

/**
 * @作者 Amos Xu
 * @版本 V1.0
 * @配置 
 * @创建日期 2016年8月22日
 * @项目名称 dsp-common
 * @描述 
 */
public class DAOException extends CommonRuntimeException{
	private static final long serialVersionUID = 1L;
	private static final String DEFAULT_MESSAGE = "处理数据库操作时发生异常";

	public DAOException() {
		this(DEFAULT_MESSAGE);
	}

	public DAOException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public DAOException(String message, Throwable cause) {
		this(message, cause,true,true);
	}

	public DAOException(String message) {
		this(message,null);
	}

	public DAOException(Throwable cause) {
		this(DEFAULT_MESSAGE,cause);
	}
}
